   simple audio scheduler - A simple GUI written in Python using the Tkinter library to make a user friendly cronjob editor mainly to play audio using command line VLC

REQUIREMENTS

In order for simple audio scheduler you need cron, vlc, and tkinter. On debian this should get you most of the way there:

apt-get install cron, vlc, python3-tk

HOW TO INSTALL

Just download the source code from gitlab.com/salvadorrye/simple_audio_scheduler and extract
anywhere

HOW TO RUN

Designed for Raspberry Pi OS, but may work on other x86 Linux distros as well; just replace the value of USER in gui2cron.py file with your Linux username. In the project directory run:

./gui2cron.py

HOW TO REMOVE CRON JOB:

Just delete /etc/cron.d/mycronjob
