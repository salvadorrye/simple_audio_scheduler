#!/usr/bin/env python3

# Simple Audio Scheduler v1.1 - A simple GUI written in Python using the Tkinter library 
# to make a user friendly cronjob editor mainly to play audio using command line VLC
# Copyright (C) 2021 Ryan Dela Rosa Salvador

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from tkinter import *
from tkinter.ttk import Frame, Button, Style
from tkinter import filedialog, BooleanVar, messagebox
from tkSimpleDialog import Dialog
import subprocess
import pickle
from pathlib import Path	

USER = 'pi'
PLAYER = 'cvlc'
TITLE='Simple Audio Scheduler'
DAYS_OF_WEEK = ['All', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
MONTHS = ['All', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
FTYPES = [('MP3 files', '*.mp3'), ('M4A files', '*.m4a'), ('WAV files', '*.wav'), ('OGG files', '*.ogg'),  ('All files', '*')]
ABOUT = "Simple Audio Scheduler v1.1 is a free software written in Python using the Tkinter library to provide \
a simple user-friendly cronjob GUI editor mainly to play audio using command line VLC\n\n\
Copyright (C) 2021 Ryan Dela Rosa Salvador\n\n\
This program is licensed under GNU GPL v3"

PROJECT_DIR = Path(__file__).resolve().parent

class PickleFileHandler(object):
    def __init__(self, filename=''):
        self.saved_state = True
        self.d = {}
        self.filename = filename
        
    def isFilenameValid(self, fname):
        if fname != '' and not isinstance(fname, tuple):
            return True
		
    def openFile(self):
        fname = filedialog.askopenfilename(filetypes=[('Pickle file', '*.pkl')])
        success = False
        if self.isFilenameValid(fname):
            try:
                f = open(fname, 'rb')  
                self.d = pickle.load(f)
                self.filename = fname
                success = True
            except IOError:
                message.showwarning('Open file', f'Cannot open this file {fname}') 
                success = False
            finally:
                f.close()
        return success        
            
    def saveFile(self):            
        if self.isFilenameValid(self.filename):
            self.saved_state = True
            f = open(self.filename, 'wb')
            pickle.dump(self.d, f)
            f.close()        
                   
    def saveAsFile(self):
        fname = filedialog.asksaveasfilename(initialfile='untitled.pkl', filetypes=[('Pickle file', '*.pkl')])
        if self.isFilenameValid(fname):
            self.filename = fname
            self.saved_state = True
            f = open(self.filename, 'wb')
            pickle.dump(self.d, f)
            f.close()

class SchedDialog(Dialog):       
    def body(self, master):
        self.fl = None            
        # Alias widgets
        frame0 = Frame(self, relief=RAISED)
        frame0.pack(fill=X)
        
        alias_lbl = Label(frame0, text='Give this entry a name', width=23)
        alias_lbl.pack(side=LEFT, padx=5, pady=5)
        
        self.alias_entry = Entry(frame0)
        self.alias_entry.pack(side=LEFT, fill=X, padx=5, expand=True)        
        
        # Audio file or playlist select widgets
        frame1 = Frame(self)
        frame1.pack(fill=X)
        
        lbl = Label(frame1, text='Audio file or VLC playlist')
        lbl.pack(anchor=W, padx=5, pady=5)
        
        browseframe = Frame(frame1)
        browseframe.pack(fill=X)
     
        browse_button = Button(browseframe, text='Browse', command=self.onOpen)
        browse_button.pack(side=LEFT, padx=5, pady=5)
                
        self.entry1 = Entry(browseframe)
        self.entry1.pack(side=LEFT, fill=X, padx=5, expand=True)
        
        # Minute widgets
        frame2 = Frame(self, relief=RAISED)
        frame2.pack(fill=X)

        lbl1 = Label(frame2, text='Minute', width=13, anchor=W)
        lbl1.pack(side=LEFT, padx=5, pady=5)

        self.spinbox0 = Spinbox(frame2, from_=0, to=59)  # Minute spinbox
        self.spinbox0.pack(side=LEFT, padx=5)

        self.var = BooleanVar()
        self.checkbut0 = Checkbutton(frame2, text='Every minute', variable=self.var, command=self.onClick)
        self.checkbut0.pack(side=LEFT, padx=5)
        
        # Hour widgets
        frame3 = Frame(self, relief=RAISED)
        frame3.pack(fill=X)

        lbl3 = Label(frame3, text='Hour', width=13, anchor=W)
        lbl3.pack(side=LEFT, padx=5, pady=5)

        self.spinbox1 = Spinbox(frame3, from_=0, to=23) # Hour spinbox
        self.spinbox1.pack(side=LEFT, padx=5)

        self.var1 = BooleanVar()
        self.checkbut1 = Checkbutton(frame3, text='Every hour', variable=self.var1, command=self.onClick)
        self.checkbut1.pack(side=LEFT, padx=5)

        # Day of month widgets
        frame4 = Frame(self, relief=RAISED)
        frame4.pack(fill=X)
        
        lbl5 = Label(frame4, text='Day of month', width=13, anchor=W)
        lbl5.pack(side=LEFT, padx=5, pady=5)

        self.spinbox2 = Spinbox(frame4, from_=1, to=31) # Day of month spinbox
        self.spinbox2.pack(side=LEFT, padx=5)
        
        self.var2 = BooleanVar()
        self.checkbut2 = Checkbutton(frame4, text='Every day', variable=self.var2, command=self.onClick)
        self.checkbut2.pack(side=LEFT, padx=5)

        # Frame 5 (contains Month and Day of Week widgets)
        frame5= Frame(self, relief=RAISED)
        frame5.pack(fill=X)

		# Month widgets
        lbl6 = Label(frame5, text='Month', width=6)
        lbl6.grid(row=0, column=0, padx=5, pady=5)
        
        sbframe = Frame(frame5, relief=RAISED)
        sbframe.grid(row=1, column=0) 
               
        scrollbar = Scrollbar(sbframe, orient=VERTICAL)
        self.lstbox0 = Listbox(sbframe, exportselection=0, yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.lstbox0.yview)
        scrollbar.pack(side=RIGHT, fill=Y)
        for i in MONTHS:
            self.lstbox0.insert(END, i)
        self.lstbox0.select_set(0)
        self.lstbox0.pack(padx=5, pady=5)

		# Day of Week widgets
        lbl7 = Label(frame5, text='Day of week', width=12)
        lbl7.grid(row=0, column=1, padx=5, pady=5)
        
        sbframe1 = Frame(frame5, relief=RAISED)
        sbframe1.grid(row=1, column=1)
        self.lstbox1 = Listbox(sbframe1, exportselection=0)
        for i in DAYS_OF_WEEK:
            self.lstbox1.insert(END, i)
        self.lstbox1.select_set(0)
        self.lstbox1.pack(padx=5, pady=5)
        
        # Right side panel widgets
        butnframe = Frame(frame5, relief=RAISED)
        butnframe.grid(row=1, column=2, sticky=N)
        
        clear_button = Button(butnframe, text='Clear', command=self.onClear)
        clear_button.pack(anchor=N, padx=5, pady=5)
        
        # If key is present it means it's in edit mode
        # therefore, we load the data
        if self.parent.key:
            self.loadData(self.parent.file_handler.d, self.parent.key)           	
        
        return self.alias_entry # initial focus
        
    def validate(self):       
        if self.alias_entry.get() != '' and self.entry1.get() != '':
            return True
        elif self.alias_entry.get() == '':
            messagebox.showerror('Error', 'Please enter an alias for this entry!')
            return False
        elif self.entry1.get() == '':
            messagebox.showerror('Error', 'Please select an audio file or playlist!')
            return False
        
    def apply(self):
        l = []     
        if self.var.get() == True:
            l.append('*')    
        else:   
            l.append(self.spinbox0.get())
        if self.var1.get() == True:
            l.append('*')    
        else:
            l.append(self.spinbox1.get())
        if self.var2.get() == True:
            l.append('*')    
        else:
            l.append(self.spinbox2.get())            
        if len(self.lstbox0.curselection()) > 0:
            if self.lstbox0.curselection()[0] > 0:
                l.append(str(self.lstbox0.curselection()[0]))
            else:
                l.append('*')
        else:
            l.append('*')			
        if len(self.lstbox1.curselection()) > 0:
            if self.lstbox1.curselection()[0] > 0:
                l.append(str(self.lstbox1.curselection()[0]-1))
            else:
                l.append('*')
        else:
            l.append('*')
        l.append(f'\'{self.fl}\'')
        
        # Save data to dictionary
        self.parent.onSaveData(self.alias_entry.get(), l)        
        self.parent.file_menu.entryconfig('Save', state=NORMAL)
        self.parent.file_handler.saved_state = False
                
    def onClick(self):
        if self.var.get() == True:
            self.spinbox0.config(state=DISABLED)    
        else:
            self.spinbox0.config(state=NORMAL)
        if self.var1.get() == True:
            self.spinbox1.config(state=DISABLED)
        else:
            self.spinbox1.config(state=NORMAL)
        if self.var2.get() == True:
            self.spinbox2.config(state=DISABLED)
        else:
            self.spinbox2.config(state=NORMAL)
            
    def onOpen(self):        
        self.dlg = filedialog.Open(self, filetypes=FTYPES)
        self.fl = self.dlg.show()

        if self.fl != '':
            self.entry1.delete(0, END)
            self.entry1.insert(0, str(self.fl))
            
    def onClear(self):
        self.alias_entry.delete(0, END)
        self.entry1.delete(0, END)
        self.spinbox0.config(state=NORMAL)
        self.spinbox0.delete(0, END)
        self.spinbox0.insert(1, '0')     
        self.spinbox1.config(state=NORMAL)
        self.spinbox1.delete(0, END)
        self.spinbox1.insert(1, '0')
        self.spinbox2.config(state=NORMAL)
        self.spinbox2.delete(0, END)
        self.spinbox2.insert(1, '1')
        self.var.set(False)
        self.var1.set(False)
        self.var2.set(False)
        
        self.lstbox0.delete(0, END)        
        for i in MONTHS:
            self.lstbox0.insert(END, i)            
           
        self.lstbox1.delete(0, END)        
        for i in DAYS_OF_WEEK:
            self.lstbox1.insert(END, i)
            
    def loadData(self, d, key):
        l = d[key]
        minute = l[0]
        hour = l[1]
        dom = l[2]
        month = l[3]
        if l[4] == '*':
            dow = '0'
        else:
            dow = str(int(l[4])+1)
        self.fl = l[5]
        self.alias_entry.insert(0, key)
        self.entry1.insert(0, self.fl)
        if minute == '*':
            self.var.set(True)
            self.spinbox0.config(state=DISABLED)
        else:
            self.spinbox0.delete(0, END)
            self.spinbox0.insert(1, minute)
        if hour == '*':
            self.var1.set(True)
            self.spinbox1.config(state=DISABLED)
        else:
            self.spinbox1.delete(0, END)
            self.spinbox1.insert(1, hour)
        if dom == '*':
            self.var2.set(True)
            self.spinbox2.config(state=DISABLED)
        else:
            self.spinbox2.delete(0, END)
            self.spinbox2.insert(1, dom)  
        if month == '*':
            self.lstbox0.select_set(0)
        else:
            self.lstbox0.delete(0, END)        
            for i in MONTHS:
                self.lstbox0.insert(END, i)
            self.lstbox0.select_set(month)
        if dow == '*':
            self.lstbox1.select_set(0)
        else:
            self.lstbox1.delete(0, END)
            for i in DAYS_OF_WEEK:
                self.lstbox1.insert(END, i)
            self.lstbox1.select_set(dow)			   

# Main application        
class App(Frame):
    def __init__(self):
        Frame.__init__(self)
        self.file_handler = PickleFileHandler()        
        self.d = {}
        self.key = None
        self.opened_file = False
        self.initUI()        

    def initUI(self):
        self.master.title(f'untitled - {TITLE}')
        self.style = Style()
        self.style.theme_use('default')
        self.pack(fill=BOTH, expand=True)
        
        # Menu bar and toolbars
        menubar = Menu(self.master)
        self.master.config(menu=menubar)
        
        self.file_menu = Menu(menubar)
        self.file_menu.add_command(label='New', command=self.onNewFile)       
        self.file_menu.add_command(label='Open', command=self.onOpenFile)
        self.file_menu.add_command(label='Save', command=self.onSaveFile, state=DISABLED)
        self.file_menu.add_command(label='Save as', command=self.onSaveAsFile)
        self.file_menu.add_separator()
        self.file_menu.add_command(label='Exit', command=self.quit)
        
        tool_menu = Menu(menubar)
        tool_menu.add_command(label='Sync time', command=self.onUpdateDatetime)
        
        about_menu = Menu(menubar)
        about_menu.add_command(label='About', command=self.onShowAbout)
        
        menubar.add_cascade(label='File', menu=self.file_menu)        
        menubar.add_cascade(label='Tools', menu=tool_menu)
        menubar.add_cascade(label='Help', menu=about_menu)
        
        # Schedule bank widgets    
        frame0 = Frame(self)
        frame0.pack(fill=X)          
        
        sched_lbl = Label(frame0, text='Schedule Bank')
        sched_lbl.grid(row=0, padx=5, pady=5)
        
        lbFrame = Frame(frame0)
        lbFrame.grid(row=1)
        
        sbar = Scrollbar(lbFrame, orient=VERTICAL)      
        self.sched_lbox = Listbox(lbFrame, exportselection=0, yscrollcommand=sbar.set)
        sbar.config(command=self.sched_lbox.yview)
        sbar.pack(side=RIGHT, fill=Y)
        
        self.onRefreshBank(self.file_handler.d)
        self.sched_lbox.bind("<Double-Button-1>", self.onSchedSelect)
        self.sched_lbox.pack(padx=5, pady=5)
        
        detail_lbl = Label(frame0, text='Detail')
        detail_lbl.grid(row=0, column=1, padx=5, pady=5)
        
        # Schedule detail widgets
        detailFrame = Frame(frame0)
        detailFrame.grid(row=1, column=1)
        
        self.minute_lbl = Label(detailFrame, text='Minute: ')
        self.minute_lbl.grid(row=0, sticky=W, padx=5, pady=5)
        
        self.hour_lbl = Label(detailFrame, text='Hour: ')
        self.hour_lbl.grid(row=1, sticky=W, padx=5, pady=5)
        
        self.dom_lbl = Label(detailFrame, text='Day of month: ')
        self.dom_lbl.grid(row=2, sticky=W, padx=5, pady=5)
        
        self.month_lbl = Label(detailFrame, text='Month: ')
        self.month_lbl.grid(row=3, sticky=W, padx=5, pady=5)
        
        self.dow_lbl = Label(detailFrame, text='Day of week: ')
        self.dow_lbl.grid(row=4, sticky=W, padx=5, pady=5)
        
        audio_lbl = Label(detailFrame, text='Audio or playlist: ')
        audio_lbl.grid(row=5, sticky=W, padx=5, pady=5)
        
        self.fname_lbl = Label(detailFrame, width=27, anchor=W)
        self.fname_lbl.grid(row=6, columnspan=2, padx=5, pady=5)
        
        # Side panel widgets (Add, Edit and Delete buttons)
        rButFrame = Frame(frame0)
        rButFrame.grid(row=1, column=2, sticky=N)
        
        add_btn = Button(rButFrame, text='Add', command=self.onAddSched)
        add_btn.grid(row=0, padx=5, pady=5)
        
        edit_btn = Button(rButFrame, text='Edit', command=self.onEditSched)
        edit_btn.grid(row=1, padx=5, pady=5)
        
        delete_btn = Button(rButFrame, text='Delete', command=self.onDeleteSched)
        delete_btn.grid(row=2, padx=5, pady=5)   
        
        # Bottom panel widgets
        kill_button = Button(self, text='Kill Audio', command=self.onKillAudio)
        kill_button.pack(side=LEFT, padx=5, pady=5)
        
        close_button = Button(self, text='Exit', command=self.validateUnsaved)
        close_button.pack(side=RIGHT, padx=5, pady=5)
        
        deploy_button = Button(self, text='Deploy', command=self.flushToCron)
        deploy_button.pack(side=RIGHT)
        
    def onNewFile(self):
        if not self.file_handler.saved_state:
            save = messagebox.askyesno('Save File', 'Save changes before closing? Your changes will be lost if you don\'t save them.')
            if save:
                self.onSaveFile()                           
        self.master.title(f'untitled - {TITLE}')       
        self.file_handler.filename = ''
        self.file_handler.d = {}
        self.onRefreshBank()
        
    def onSaveFile(self):
        self.file_handler.saveFile()
        if self.file_handler.saved_state:
            self.file_menu.entryconfig('Save', state=DISABLED)
        else:
            self.onSaveAsFile()
        
    def onSaveAsFile(self):
        self.file_handler.saveAsFile()
        if self.file_handler.saved_state:
            self.file_menu.entryconfig('Save', state=DISABLED)
            fname = self.file_handler.filename.split('/')[-1]
            self.master.title(f'{fname} - {TITLE}')     
        
    def onSaveData(self, k, v):
        self.file_handler.d[k] = v
        self.onRefreshBank(self.file_handler.d) 
        
    def onRefreshBank(self, d={}):
        self.sched_lbox.delete(0, END)
        for k in list(d.keys()):
            self.sched_lbox.insert(END, k)
        
    def onOpenFile(self):
        if not self.file_handler.saved_state:
            save = messagebox.askyesno('Save File', 'Save changes before making a new file? Your changes will be lost if you don\'t save them.')
            if save:
                self.onSaveFile()
        else:
            self.opened_file = self.file_handler.openFile()
            if self.opened_file:
                self.onRefreshBank(self.file_handler.d)
                fname = self.file_handler.filename.split('/')[-1]
                self.master.title(f'{fname} - {TITLE}')
                
    def onUpdateDatetime(self):
        subprocess.call([f'{PROJECT_DIR}/update_datetime'])
    
    def onShowAbout(self):
        top = Toplevel()
        top.title('About Simple Audio Scheduler')
        msg = Message(top, text=ABOUT)
        msg.pack(padx=5, pady=5)       
        button = Button(top, text='Dismiss', command=top.destroy)
        button.pack(padx=5, pady=5)
        
    def onSchedSelect(self, b=None):
        if len(self.sched_lbox.curselection()) > 0:
            l = self.file_handler.d[self.sched_lbox.get(self.sched_lbox.curselection())]
            self.minute_lbl.config(text=f'Minute: {l[0]}')
            self.hour_lbl.config(text=f'Hour: {l[1]}')
            self.dom_lbl.config(text=f'Day of month: {l[2]}')
            self.month_lbl.config(text=f'Month: {l[3]}')
            self.dow_lbl.config(text=f'Day of week: {l[4]}') 
            s = l[5].split("/").pop()  
            fn = (s[:25] + '.."') if len(s) > 28 else s     
            self.fname_lbl.config(text=f'"{fn}')
        
    def onAddSched(self):  
        self.key = None      
        SchedDialog(self, 'Add Schedule')
        
    def onEditSched(self):
        if len(self.sched_lbox.curselection()) > 0:
            self.d = self.file_handler.d
            self.key = self.sched_lbox.get(self.sched_lbox.curselection()) 
            SchedDialog(self, 'Edit Schedule')
        
    def onDeleteSched(self):
        if len(self.sched_lbox.curselection()) > 0:
            key = self.sched_lbox.get(self.sched_lbox.curselection())
            delete = messagebox.askyesno('Delete', f'Are you sure you want to delete "{key}?"')
            if delete:
                self.sched_lbox.delete(self.sched_lbox.curselection())
                self.file_handler.d.pop(key, None)
            
    def onKillAudio(self):
        subprocess.call(['pkill', 'vlc'])
        
    def translate_to_cron_line(self, lst):
        return f'{lst[0]} {lst[1]} {lst[2]} {lst[3]} {lst[4]} {USER} {PLAYER} {lst[5]}\n'
        
    def flushToCron(self):
        f = open('mycronjob', 'w')     
        d = self.file_handler.d   
        for k in d.keys():
            line = self.translate_to_cron_line(d[k])
            f.write(line)
        f.close()
        subprocess.call(['sudo', 'cp', 'mycronjob', '/etc/cron.d/'])
        
    def validateUnsaved(self):       
        if not self.file_handler.saved_state:
            save = messagebox.askyesno('Save File', 'Save changes before closing? Your changes will be lost if you don\'t save them.')
            if not save:
                self.quit()
            else:
                if self.opened_file:
                    self.onSaveFile()
                else:
                    self.onSaveAsFile()
        else:
            self.quit()
        
if __name__ == '__main__': 
    root = Tk()
    root.geometry('520x300+300+300')
    app = App()
    root.protocol('WM_DELETE_WINDOW', app.validateUnsaved)    
    root.mainloop()
